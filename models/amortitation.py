# -*- coding: utf-8 -*-
from odoo import api, fields, models
from datetime import date
import logging
from odoo.exceptions import AccessError, UserError, ValidationError

_logger = logging.getLogger(__name__)


class SaleAmortization(models.Model):
    _name = 'sale.amortization'

    numero_pago = fields.Integer(string="Núm. pago")
    saldo_inicial = fields.Float(string="Saldo inicial")
    pago_capital = fields.Float(string="Capital")
    pago_intereses = fields.Float(string="Interés")
    pago_realizado = fields.Float(string="Pago a realizar")
    resto_pago = fields.Float(string="Resta pago", compute="_compute_resto_pago")
    is_parcial = fields.Boolean(string="¿Es un pago parcial?", compute="_compute_is_parcial")
    saldo_capital_despues_pago = fields.Float(string="Saldo final")
    fecha_pago = fields.Date(string="Fecha de pago")
    order_id = fields.Many2one(
        'sale.order',
        string="Cotización"
    )
    abono_capital = fields.Float(string="Abono capital opcional")
    # hchavez agrego 2 campos para ligado de anticipo
    ant_id = fields.Many2one('account.move', string="Anticipo")
    ant_state = fields.Selection(related='ant_id.state', string="Estado del Anticipo", store=True, readonly=True)
    interes_moratorio = fields.Float(string="Interés moratorio", compute="_compute_interes_moratorio")
    # hchavez agrego 2 campos para ligado de anticipo a capital
    acap_id = fields.Many2one('account.move', string="Documento Abono a Capital")
    acap_state = fields.Selection(related='acap_id.state', string="Estado del Abono a Capital", store=True, readonly=True)
    # hchavez agrego 2 campos para ligado de intereses moratorios
    imor_id = fields.Many2one('account.move', string="Documento Intereses Moratorios")
    imor_state = fields.Selection(related='imor_id.state', string="Estado de Intereses Moratorios", store=True, readonly=True)

    monto_pago = fields.Float(string="Pago", compute="_compute_monto_pago", store=True)
    pago_socio = fields.Boolean(related="order_id.pago_socio" )

    def _compute_is_parcial(self):
        for record in self:
            if round(record.resto_pago, 2) > 0 and record.ant_id:
                record.is_parcial = True
            else:
                record.is_parcial = False

    def _compute_resto_pago(self):
        for record in self:
            if record.ant_id:
                record.resto_pago = abs(round(record.monto_pago, 2) - round(record.ant_id.amount_total_signed, 2))
            else:
                record.resto_pago = record.monto_pago

    def _compute_interes_moratorio(self):
        today = fields.date.today()
        for record in self:
            fecha_pago = record.fecha_pago
            if not record.ant_id:
                dias_restantes = (fecha_pago - today).days
            else:
                fecha_ultimo_pago = record.ant_id.fecha_cobro if record.ant_id.fecha_cobro else today
                dias_restantes = (fecha_pago - fecha_ultimo_pago).days
            if dias_restantes < 0 and abs(dias_restantes) > 5:
                record.interes_moratorio = abs(dias_restantes) * ((record.monto_pago * 0.05) / 30)
            else:
                record.interes_moratorio = 0.0

    @api.depends("pago_capital", "pago_intereses")
    def _compute_monto_pago(self):
        for record in self:
            record.monto_pago = record.pago_capital + record.pago_intereses

    # hchavez esta función crea el anticipo con las cantidades calculadas en la amortizacion
    def gen_ant(self):
        today = date.today()
        lines = []
        vals = {}
        p_id = self.env['sale.advance.payment.inv']._default_product_id()
        a_id = self.env['sale.advance.payment.inv']._default_deposit_account_id()
        t_id = self.env['sale.advance.payment.inv']._default_deposit_taxes_id()
        if self.pago_realizado:
            monto_pago = self.pago_realizado
        else:
            monto_pago = self.monto_pago
        self.pago_realizado = 0
        line_ant = {
            'product_id': p_id.id,
            'quantity': '1.0',
            'price_unit': monto_pago,
            'tax_ids': [(6, 0, t_id.ids)],
            'name': p_id.name,
            'account_id': a_id.id,
            'product_uom_id': p_id.uom_id.id if p_id.uom_id else None
        }
        lines.append((0, 0, line_ant))
        vals = {
            'state': 'draft',
            'partner_id': self.order_id.partner_id.id,
            'partner_shipping_id': self.order_id.partner_shipping_id,
            'invoice_date': today,
            'fecha_cobro': today,
            'invoice_origin': self.order_id.name,
            'move_type': 'out_invoice',  # cambiamos el tipo de factura a anticipo
            'invoice_line_ids': lines,  # eliminamos las lineas default ya que se crearan a mano
            'inv_type': 'a'  # no marcamos como t para que no le aparezca el boton de anticipos
        }
        self.ant_id = self.env['account.move'].create(vals)
        #hchavez intentamos cambiar la cuenta contable por default
        cdef = self.order_id.partner_id.property_account_receivable_id
        _logger.error('cuenta default'+str(cdef))
        cnew = self.order_id.partner_id.adv_account
        _logger.error('cuenta nueva'+str(cnew))
        for l in self.ant_id.line_ids:
          _logger.error('cuenta a analizar->'+str(l.account_id)+' cuenta default '+ str(cdef))
          if l.account_id == cdef:
              l.account_id = cnew
              self.env.cr.commit()
        #fin de la modificación        
        return self

    # hchavez esta función crea el anticipo por los intereses moratorios
    def gen_imor(self):
        today = date.today()
        lines = []
        vals = {}
        #obtiene el producto id configurado para intereses moratorios
        product_id = self.env['ir.config_parameter'].sudo().get_param('sale.default_intmor_product_id')
        p_id =  self.env['product.product'].browse(int(product_id)).exists()
        line_ant = {
            'product_id': p_id.id,
            'quantity': '1.0',
            'price_unit': self.interes_moratorio,
#           'tax_ids': [(6, 0, t_id.ids)],
            'name': p_id.name,
#           'account_id': a_id.id,
            'product_uom_id': p_id.uom_id.id if p_id.uom_id else None
        }
        lines.append((0, 0, line_ant))
        vals = {
            'state': 'draft',
            'partner_id': self.order_id.partner_id.id,
            'partner_shipping_id': self.order_id.partner_shipping_id,
            'invoice_date': today,
            'fecha_cobro': today,
            'invoice_origin': self.order_id.name,
            'move_type': 'out_invoice',  # cambiamos el tipo de factura a anticipo
            'invoice_line_ids': lines,  # eliminamos las lineas default ya que se crearan a mano
            'inv_type': 'a'  # no marcamos como t para que no le aparezca el boton de anticipos
        }
        self.imor_id = self.env['account.move'].create(vals)
        return self

    # hchavez esta función crea el anticipo por el anticipo a capital
    def gen_acap(self):
        today = date.today()
        lines = []
        vals = {}
        #obtiene el producto id configurado para intereses moratorios
        product_id = self.env['ir.config_parameter'].sudo().get_param('sale.default_abocap_product_id')
        p_id =  self.env['product.product'].browse(int(product_id)).exists()
        line_ant = {
            'product_id': p_id.id,
            'quantity': '1.0',
            'price_unit': self.abono_capital,
#           'tax_ids': [(6, 0, t_id.ids)],
            'name': p_id.name,
#           'account_id': a_id.id,
            'product_uom_id': p_id.uom_id.id if p_id.uom_id else None,
        }
        lines.append((0, 0, line_ant))
        vals = {
            'state': 'draft',
            'partner_id': self.order_id.partner_id.id,
            'partner_shipping_id': self.order_id.partner_shipping_id,
            'invoice_date': today,
            'fecha_cobro': today,
            'invoice_origin': self.order_id.name,
            'move_type': 'out_invoice',  # cambiamos el tipo de factura a anticipo
            'invoice_line_ids': lines,  # eliminamos las lineas default ya que se crearan a mano
            'inv_type': 'a'  # no marcamos como t para que no le aparezca el boton de anticipos
        }
        self.acap_id = self.env['account.move'].create(vals)
        return self




    # hchavez esta función crea el anticipo con las cantidades calculadas en la amortizacion
    def gen_ant_parcial(self):
        p_id = self.env['sale.advance.payment.inv']._default_product_id()
        a_id = self.env['sale.advance.payment.inv']._default_deposit_account_id()
        t_id = self.env['sale.advance.payment.inv']._default_deposit_taxes_id()
        line_ant = {
            'product_id': p_id.id,
            'quantity': '1.0',
            'price_unit': self.pago_realizado if self.pago_realizado > 0 else self.resto_pago,
            'tax_ids': [(6, 0, t_id.ids)],
            'name': p_id.name,
            'account_id': a_id.id,
            'product_uom_id': p_id.uom_id.id if p_id.uom_id else None
        }
        self.ant_id.invoice_line_ids = [(0, 0, line_ant)]
        self.pago_realizado = 0.0

    def write(self, vals):
        res = super(SaleAmortization, self).write(vals)
        if 'stop_write_recursion' not in self.env.context:
            self.with_context(stop_write_recursion=1).order_id.add_update_tabla()
        return res
