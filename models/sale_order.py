# -*- coding: utf-8 -*-
from odoo import api, fields, models
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
import numpy_financial as npf


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    enganche_parcial = fields.Float(string="Enganche %", )
    apartado = fields.Float(string="Apartado", )
    saldo_enganche = fields.Float(string="Saldo enganche", )
    limit_pago_eng = fields.Date(string="Límite pago enganche", )
    total_financiar = fields.Float(string="Total a financiar", )
    primer_mensualidad = fields.Date(string="1er mensualidad", compute="_compute_primer_mensualidad", )
    mensualidad_0 = fields.Float(string="Mensualidad 0%", compute="_compute_mensualidad_0", )
    mensualidad_1 = fields.Float(string="Mensualidad 1%", compute="_compute_for_mensualidad_1", )
    amortitation_id = fields.One2many('sale.amortization', 'order_id', string="Tabla de Amortización", )
    tipo_mesualidad = fields.Selection([
        ("24", "24 meses"),
        ("60", "60 meses"),
        ("120", "120 meses")
    ], string="Tipo de mensualidad", default="60")
    tipo_enganche = fields.Float(string="Tipo de enganche" )
    tipo_categoria = fields.Selection([
        ("libelula", "Libélula"),
        ("socio_const", "Socio constructor"),
        ("residencial", "Residencial"),
        ("alzare", "Alzare")
    ], string="Tipo de categoría")
    interes_mensual = fields.Float(string="Interés mensual", default=0.01)
    pago_socio = fields.Boolean(string="¿Se registró un pago de socio?", compute="_compute_pago_socio")
    fecha_contrato = fields.Date(string="Fecha contrato", )
    enganche_account_id = fields.Many2one('account.move', string="Pago enganche", copy=False)
    enganche_pagado = fields.Boolean(string="Enganche pagado", compute="_compute_enganche_pagado")

    def _compute_enganche_pagado(self):
        for record in self:
            if self.enganche_parcial == round(record.enganche_account_id.amount_total_signed, 2):
                record.enganche_pagado = True
            else:
                record.enganche_pagado = False

    @api.onchange("tipo_categoria")
    def onchange_tipo_categoria(self):
        if self.tipo_categoria == "libelula":
            self.tipo_enganche = 15
            self.interes_mensual = 0.01
            self.tipo_mesualidad = "120"
        elif self.tipo_categoria == "alzare":
            self.tipo_enganche = 10
            self.interes_mensual = 0.01
            self.tipo_mesualidad = "60"
        elif self.tipo_categoria == "residencial":
            self.tipo_enganche = 15
            self.interes_mensual = 0.01
            self.tipo_mesualidad = "60"
        elif self.tipo_categoria == "socio_const":
            self.tipo_enganche = 20
            self.interes_mensual = (10/12)/100
            self.tipo_mesualidad = "24"

    @api.depends("fecha_contrato")
    def _compute_primer_mensualidad(self):
        for record in self:
            if record.fecha_contrato:
                record.primer_mensualidad = record.fecha_contrato + relativedelta(months=1)
            else:
                record.primer_mensualidad = 0

    def _compute_pago_socio(self):
        for record in self:
            if record.amortitation_id.filtered(lambda x: x.ant_id) and record.tipo_categoria == "socio_const":
                record.pago_socio = True
            else:
                record.pago_socio = False

    @api.depends("mensualidad_0", "amortitation_id")
    def _compute_for_mensualidad_1(self):
        for record in self:
            if record.amortitation_id and len(record.amortitation_id) >= 24:
                mensualidad_24 = record.amortitation_id.filtered(lambda x: x.numero_pago == 24)
                inicial = mensualidad_24.saldo_capital_despues_pago
            else:
                inicial = record.total_financiar - (record.mensualidad_0 * 24)
            if int(record.tipo_mesualidad) < 25:
                record.mensualidad_1 = 0.0
            else:
                mensualidad_1 = npf.pmt(record.interes_mensual, int(record.tipo_mesualidad) - 24, -inicial, 0)
                record.mensualidad_1 = mensualidad_1 if mensualidad_1 >= 0 else 0.0

    @api.depends("tipo_mesualidad", "total_financiar")
    def _compute_mensualidad_0(self):
        for record in self:
            mil = 0
            if record.tipo_categoria == "libelula":
                mil = 1000
            if record.tipo_mesualidad:
                record.mensualidad_0 = (record.total_financiar / int(record.tipo_mesualidad)) + mil
            else:
                record.mensualidad_0 = 0

    @api.onchange("amount_total", "tipo_enganche")
    def onchangue_amount_total(self):
        self.enganche_parcial = self.amount_total * (self.tipo_enganche/100)
        self.total_financiar = self.amount_total - self.enganche_parcial
        self.saldo_enganche = self.enganche_parcial - self.apartado

    @api.onchange("apartado")
    def onchangue_apartado(self):
        self.saldo_enganche = self.enganche_parcial - self.apartado

    def clear_table(self):
        self.amortitation_id = [(5, 0, 0)]

    def generate_amortization_socio_constructor(self):
        vals_list = []
        self.clear_table()
        saldo_inicial = self.total_financiar
        plazos = int(self.tipo_mesualidad)
        pago_intereses = saldo_inicial * self.interes_mensual
        for x in range(plazos):
            fecha_pago = self.fecha_contrato + relativedelta(months=x + 1)
            num_pago = x + 1
            saldo_capital_despues_pago = saldo_inicial + pago_intereses
            vals_dict = {
                'fecha_pago': fecha_pago,
                'numero_pago': num_pago,
                'saldo_inicial': saldo_inicial,
                'pago_intereses': pago_intereses,
                'saldo_capital_despues_pago': saldo_capital_despues_pago,
                'pago_capital': saldo_inicial,
            }
            vals_list.append(vals_dict)
            saldo_inicial = saldo_capital_despues_pago
        self.amortitation_id = [(0, 0, x) for x in vals_list]

    def generate_amortization_lib_res(self):
        vals_list = []
        self.clear_table()
        saldo_inicial = self.total_financiar
        mensualidad = self.mensualidad_0
        plazos = int(self.tipo_mesualidad)
        for x in range(plazos):
            fecha_pago = self.fecha_contrato + relativedelta(months=x + 1)
            num_pago = x + 1
            if num_pago < 25:
                pago_intereses = 0.0
                pago_capital = mensualidad
            else:
                pago_intereses = saldo_inicial * self.interes_mensual
                pago_capital = self.mensualidad_1 - pago_intereses
            saldo_capital_despues_pago = saldo_inicial - pago_capital
            if saldo_capital_despues_pago < 0:
                pago_capital = saldo_inicial
                pago_intereses = saldo_inicial * self.interes_mensual
                saldo_capital_despues_pago = pago_capital + pago_intereses
            vals_dict = {
                'fecha_pago': fecha_pago,
                'numero_pago': num_pago,
                'saldo_inicial': saldo_inicial,
                'pago_capital': pago_capital,
                'pago_intereses': pago_intereses,
                'saldo_capital_despues_pago': saldo_capital_despues_pago,
            }
            vals_list.append(vals_dict)
            saldo_inicial = saldo_inicial - pago_capital
        self.amortitation_id = [(0, 0, x) for x in vals_list]

    def generate_amortization(self):
        for record in self:
            record.validate_anticipos()
            if record.tipo_categoria == "socio_const":
                record.generate_amortization_socio_constructor()
            else:
                record.generate_amortization_lib_res()

    def add_update_tabla(self):
        for record in self:
            linea_anterior = None
            for line in record.amortitation_id:
                if linea_anterior:
                    if linea_anterior.saldo_capital_despues_pago <= 0:
                        line.pago_intereses = line.pago_capital = line.saldo_capital_despues_pago = 0
                        line.saldo_inicial = 0
                        linea_anterior = line
                        continue
                    else:
                        line.saldo_inicial = linea_anterior.saldo_capital_despues_pago
                        if line.numero_pago >= 25:
                            line.pago_intereses = line.saldo_inicial * self.interes_mensual
                            line.pago_capital = record.mensualidad_1 - line.pago_intereses
                        else:
                            line.pago_capital = record.mensualidad_0
                    linea_anterior = line
                line.saldo_capital_despues_pago = line.saldo_inicial - line.abono_capital - line.pago_capital
                if line.saldo_capital_despues_pago < 0:
                    line.pago_capital = line.saldo_inicial
                    line.saldo_capital_despues_pago = line.saldo_inicial - line.abono_capital - line.pago_capital
                if not linea_anterior:
                    linea_anterior = line

    def write(self, vals):
        res = super(SaleOrder, self).write(vals)
        if not self._context.get('generate_amortization'):
            self.with_context(stop_write_recursion=1).add_update_tabla()
        return res

    def validate_anticipos(self):
        if self.amortitation_id.filtered(lambda x: x.ant_id):
            raise ValidationError("¡No es posible crear una nueva tabla con anticipos creados!")

    def action_view_amortitation(self):
        return self._get_action_view_amortitation(self.amortitation_id)

    def _get_action_view_amortitation(self, amortitation_id):
        action = self.env["ir.actions.actions"]._for_xml_id("custom_sale.action_amortitation_tree_all")
        action['domain'] = [('id', 'in', amortitation_id.ids)]
        return action

    def generar_pago_apartado(self):
        today = fields.date.today()
        lines = []
        p_id = self.env['sale.advance.payment.inv']._default_product_id()
        a_id = self.env['sale.advance.payment.inv']._default_deposit_account_id()
        t_id = self.env['sale.advance.payment.inv']._default_deposit_taxes_id()
        monto_pago = self.apartado
        line_ant = {
            'product_id': p_id.id,
            'quantity': '1.0',
            'price_unit': monto_pago,
            'tax_ids': [(6, 0, t_id.ids)],
            'name': p_id.name,
            'account_id': a_id.id,
            'product_uom_id': p_id.uom_id.id if p_id.uom_id else None
        }
        lines.append((0, 0, line_ant))
        vals = {
            'state': 'draft',
            'partner_id': self.partner_id.id,
            'partner_shipping_id': self.partner_shipping_id,
            'invoice_date': today,
            'fecha_cobro': today,
            'invoice_origin': self.name,
            'move_type': 'out_invoice',
            'invoice_line_ids': lines,
            'inv_type': 'a',
        }
        self.enganche_account_id = self.env['account.move'].create(vals)
        cdef = self.partner_id.property_account_receivable_id
        cnew = self.partner_id.adv_account
        for l in self.enganche_account_id.line_ids:
            if l.account_id == cdef:
                l.account_id = cnew
                self.env.cr.commit()

    def gen_saldo_enganche_resto(self):
        p_id = self.env['sale.advance.payment.inv']._default_product_id()
        a_id = self.env['sale.advance.payment.inv']._default_deposit_account_id()
        t_id = self.env['sale.advance.payment.inv']._default_deposit_taxes_id()
        line_ant = {
            'product_id': p_id.id,
            'quantity': '1.0',
            'price_unit': self.saldo_enganche,
            'tax_ids': [(6, 0, t_id.ids)],
            'name': p_id.name,
            'account_id': a_id.id,
            'product_uom_id': p_id.uom_id.id if p_id.uom_id else None
        }
        self.enganche_account_id.invoice_line_ids = [(0, 0, line_ant)]
