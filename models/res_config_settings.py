# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
    intmor_default_product_id = fields.Many2one(
        'product.product',
        'Intereses Moratorios',
        domain="[('type', '=', 'service')]",
        config_parameter='sale.default_intmor_product_id',
        help='Default product used for default interest')
    
    abocap_default_product_id = fields.Many2one(
        'product.product',
        'Abono a Capital',
        domain="[('type', '=', 'service')]",
        config_parameter='sale.default_abocap_product_id',
        help='Default product used for Capital subscription')
