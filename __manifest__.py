# -*- coding: utf-8 -*-

{
    'name': 'custom_sale',
    'version': '15.0',
    'description': 'Añade funcionalidades de ventas',
    'author': 'Meridasoft',
    'license': 'LGPL-3',
    'category': '',
    'depends': [
        'ali-accounting',
        'sale_management',
    ],
    'data': [
        'security/ir.model.access.csv',
        'report/report_saleorder.xml',
        'views/sale_order_lines.xml',
        'views/sale_order.xml',
        'views/amortitation_views.xml',
        'views/res_config_settings.xml',
    ],
    # 'images': ['static/src/img/icon.png'],
    # only loaded in demonstration mode
    'demo': [
    ],
    'auto_install': False,
    'application': True,
    'installable': True,
}
